from datetime import datetime as dt
from datetime import timedelta
import os
import time


def setup():
    """
    Sets up needed files and configurations.
    :ret: Config values.
    """

    try:
        cfg_file = open("./.pomodoro", "r")
        lines = cfg_file.readlines()
        try:
            po_time = int(lines[0].split("=")[1].replace("\n", ""))
            s_break = int(lines[1].split("=")[1].replace("\n", ""))
            l_break = int(lines[2].split("=")[1].replace("\n", ""))
            l_break_freq = int(lines[3].split("=")[1].replace("\n", ""))
        except (ValueError, IndexError):
            print("whoops, somethings crashed! let's set it up again.")
            os.remove("./.pomodoro")
            return setup()
    except IOError:
        cfg_text = "POMODORO_TIME={}\nSHORT_BREAK={}\
        \nLONG_BREAK={}\nBREAK_FREQ={}"
        cfg_file = open("./.pomodoro", "w")
        print("a few information before we start.")
        po_time = input("pomodoro time (minutes): ")
        s_break = input("short break time (minutes): ")
        l_break = input("long break time (minutes): ")
        l_break_freq = input("how many pomodoros between long breaks: ")
        cfg_file.write(
            cfg_text.format(po_time, s_break, l_break, l_break_freq)
        )
        print("k, saved.")

    return (int(po_time), int(s_break), int(l_break), int(l_break_freq))


def print_menu():
    message = """
~~~~~~~~~~~~~~~~~~~~~~~~~
~   0)  start to work   ~
~   1)  daily summary   ~
~   2)  history         ~
~   3)  setup times     ~
~   q)  quit            ~
~~~~~~~~~~~~~~~~~~~~~~~~~

"""
    choice = input(message)
    if choice not in ("0", "1", "2", "3", "q"):
        print("choose a valid one please")
        return print_menu()

    return choice


def pomodoro(mins):
    """Sets a pomodoro for [mins] minutes."""

    def beep():
        os.system("echo -n '\a';sleep 0.15;"*4)

    os.system("echo -n '############ pomodoro starting ###########\n'")
    beep()
    for min in range(mins):
        os.system("echo -n '\r{} minutes to break.'".format(mins-min))
        time.sleep(60)
    os.system("echo -n '\r0 minutes to break.\n'")
    beep()


def break_time(mins):
    """Sets a break for [mins] minutes."""
    os.system("echo -n '############ break time ###########\n'")
    for min in range(mins):
        os.system("echo -n '\r{} minutes to start.'".format(mins - min))
        time.sleep(60)
    os.system("echo -n '\r0 minutes to start.\n'")


def write_to_history(pds=1):
    """Write this pomodoro to work history."""

    try:
        hist_file = open("./.work_history", "r+")
    except IOError:
        hist_file = open("./.work_history", "wr+")

    today = dt.today().strftime("%b %d, %Y %A") + "\n"
    lines = hist_file.readlines()
    if today in lines:
        ix = lines.index(today)
        temp = int(lines[ix+1].replace("\n", ""))
        lines[ix+1] = str(temp + pds) + "\n"
        hist_file = open("./.work_history", "w")
        hist_file.writelines(lines)
    else:
        new_lines = [today, str(pds)+"\n", "\n"]
        hist_file.writelines(new_lines)

    hist_file.close()


def show_todays_work():
    """Show todays work summary."""

    try:
        hist_file = open("./.work_history", "r")
    except IOError:
        print("we could not find any work history data.")

    today = dt.today().strftime("%b %d, %Y %A") + "\n"
    lines = hist_file.readlines()
    if today in lines:
        ix = lines.index(today)
        pds = lines[ix+1].replace("\n", "")
        print("\n### you have {} pomodoros today. good job! ###\n".format(pds))
    else:
        print("\n### seems like you haven't started to work today. ###\n")


def print_history(days):
    """Show work history for [days] days."""

    try:
        hist_file = open("./.work_history", "r")
    except IOError:
        print("we could not find any work history data.")
        return

    message = "Here is your work history for {} days: \n".format(days)
    message += "~"*20 + "\n"
    lines = hist_file.readlines()
    hist_file.close()
    day = dt.today() - timedelta(days=days-1)
    while day <= dt.today():
        day_str = day.strftime("%b %d, %Y %A") + "\n"
        if day_str in lines:
            ix = lines.index(day_str)
            pds = lines[ix+1].replace("\n", "")
        else:
            pds = 0
        message += "\n{} pomodoros at {}".format(pds, day_str)
        day += timedelta(days=1)
    print(message)


def main():
    """Run the program."""
    (po_time, s_break, l_break, l_break_freq) = setup()
    while 1:
        choice = print_menu()

        if choice == "0":
            done = 0
            while 1:
                pomodoro(po_time)
                write_to_history()
                done += 1
                if done == l_break_freq:
                    done = 0
                    break_time(l_break)
                else:
                    break_time(s_break)

        elif choice == "1":
            show_todays_work()

        elif choice == "2":
            try:
                days = int(input("How many days you want to see?\n"))
            except (ValueError, TypeError):
                print("unexpected behaviour.")
                continue
            print_history(days)

        elif choice == "3":
            os.remove("./.pomodoro")
            return main()

        elif choice == "q":
            print("bye!")
            return
        else:
            print("whoops, you found a bug.")


main()
