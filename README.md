# POMODORO Application for daily usage

- Keeps your work history
- Sets timer for you and give alerts at break times.
- Long breaks after some pomodoros
- Customizable pomodoro and break time
- Shows remaining minutes


# Usage

Download the repository to <path>/pomodoro/
Run "python3 pomodoro.py"
Set times as you want and start to use

